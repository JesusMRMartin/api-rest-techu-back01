package com.techu.backend.controller;
import com.techu.backend.model.ProductoModel;
import com.techu.backend.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import java.util.List;
@RestController
@RequestMapping("${url.base}")
public class ProductoController {
    @Autowired
    private ProductoService productoService;
    @GetMapping("")
    public String home(){
        return "API REST Tech U! v1.0.0";
    }
    // GET todos los productos (collection)
    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.getProductos();
    }
    // GET a un único producto por ID (instancia)
    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoId(@PathVariable int id) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }
    // POST para crear un producto
    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(@RequestBody ProductoModel productoModel) {
        productService.addProducto(productoModel);
        return new ResponseEntity<>("Product created successfully!", HttpStatus.CREATED);
    }
    // PUT para actualizar un producto
    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody ProductoModel productToUpdate) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.updateProducto(id-1, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK); // Buenas prácticas dicen que mejor enviar 204 No Content
    }
    // GET subrecurso
    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable int id){
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsers()!=null)
            return ResponseEntity.ok(pr.getUsers());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    // DELETE para borrar un producto
    DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable Integer id) {
        ProductoModel pr = productService.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.removeProducto(id - 1);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); // status code 204-No Content
    }
    // PATCH para actualizar parcialmente un producto
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(
            @RequestBody ProductoPrecioOnly productoPrecioOnly, @PathVariable int id){
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(productoPrecioOnly.getPrecio());
        productService.updateProducto(id-1, pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }
}