package com.techu.backend.service;
import com.techu.backend.model.ProductoModel;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
@Component
public class ProductoService {
    private List<ProductoModel> productoList = new ArrayList<>();
    public ProductoService(){
        productoList.add(new ProductoModel("1", "producto 1", 100.50));
        productoList.add(new ProductoModel("2", "producto 2", 150.00));
        productoList.add(new ProductoModel("3", "producto 3", 100.00));
        productoList.add(new ProductoModel("4", "producto 4", 50.50));
        productoList.add(new ProductoModel("5", "producto 5", 103.50));
    }
    // READ productos
    public List<ProductoModel> getProductos(){
        return productoList;
    }
    // READ instance (por ID)
    public ProductoModel getProductoById(int id){
        return productoList.get(id);
    }
    // CREATE productos
    public ProductoModel addProducto(ProductoModel nuevoProducto) {
        productoList.add(nuevoProducto);
        return nuevoProducto;
    }
}
